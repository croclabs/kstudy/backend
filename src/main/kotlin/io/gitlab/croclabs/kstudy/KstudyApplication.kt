package io.gitlab.croclabs.kstudy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KstudyApplication

fun main(args: Array<String>) {
	runApplication<KstudyApplication>(*args)
}
