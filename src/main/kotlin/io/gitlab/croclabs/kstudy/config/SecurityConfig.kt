package io.gitlab.croclabs.kstudy.config

import org.springframework.boot.autoconfigure.security.servlet.PathRequest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.SecurityFilterChain


@Configuration
class SecurityConfig {

	@Bean
	fun filterChain(http: HttpSecurity): SecurityFilterChain {
		return http
			.authorizeHttpRequests {
				it
					.requestMatchers("/login").permitAll()
					.anyRequest().authenticated()
			}
			.formLogin(Customizer.withDefaults())
			.csrf {
				it.ignoringRequestMatchers(PathRequest.toH2Console())
			}.headers {
				it.frameOptions {it2 ->
					it2.sameOrigin()
				}
			}.build()
	}

	@Bean
	fun passwordEncoder(): PasswordEncoder {
		return BCryptPasswordEncoder()
	}
}