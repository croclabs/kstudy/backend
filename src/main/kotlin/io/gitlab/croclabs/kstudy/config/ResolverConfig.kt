package io.gitlab.croclabs.kstudy.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.FileTemplateResolver

@Configuration
class ResolverConfig {
	@Bean
	fun secondaryTemplateResolver(): FileTemplateResolver {
		val secondaryTemplateResolver = FileTemplateResolver()
		secondaryTemplateResolver.prefix = "./templates/"
		secondaryTemplateResolver.suffix = ".html"
		secondaryTemplateResolver.templateMode = TemplateMode.HTML
		secondaryTemplateResolver.characterEncoding = "UTF-8"
		secondaryTemplateResolver.order = 1
		secondaryTemplateResolver.checkExistence = true

		return secondaryTemplateResolver
	}
}
