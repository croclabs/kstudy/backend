package io.gitlab.croclabs.kstudy

import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile
import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfileRepository
import io.gitlab.croclabs.kstudy.security.user
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/test")
class TestController(
	private val userProfileRepository: UserProfileRepository
) {
	@GetMapping
	fun currentUser(): UserProfile {
		return user()
	}
}