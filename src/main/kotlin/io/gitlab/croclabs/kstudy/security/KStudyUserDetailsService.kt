package io.gitlab.croclabs.kstudy.security

import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfileRepository
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
class KStudyUserDetailsService(
	private val userProfileRepository: UserProfileRepository
) : UserDetailsService {

	override fun loadUserByUsername(username: String?): UserDetails {
		return KStudyUserDetails(userProfileRepository.findByUsername(username))
	}

}