package io.gitlab.croclabs.kstudy.security

import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile
import org.aspectj.weaver.tools.cache.SimpleCacheFactory.enabled
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class KStudyUserDetails(
	val userProfile: UserProfile
) : UserDetails {
	override fun getAuthorities(): Collection<GrantedAuthority> = userProfile.authorities
	override fun getPassword(): String = userProfile.password
	override fun getUsername(): String = userProfile.username
	override fun isAccountNonExpired(): Boolean = !userProfile.expired
	override fun isAccountNonLocked(): Boolean = !userProfile.locked
	override fun isCredentialsNonExpired(): Boolean = !userProfile.credentialsExpired
	override fun isEnabled(): Boolean = userProfile.enabled
}