package io.gitlab.croclabs.kstudy.security

import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile
import org.springframework.security.core.context.SecurityContextHolder

fun user(): UserProfile {
	return (SecurityContextHolder.getContext().authentication.principal as KStudyUserDetails).userProfile
}