package io.gitlab.croclabs.kstudy.advices

import jakarta.servlet.http.HttpServletRequest
import org.springframework.core.env.Environment
import org.springframework.core.env.get
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.resource.NoResourceFoundException
import org.thymeleaf.exceptions.TemplateInputException

@ControllerAdvice
class IndexControllerAdvice(
	private val environment: Environment,
	private val request: HttpServletRequest
) {

	@ExceptionHandler(NoResourceFoundException::class)
	fun index(): String {
		if (environment["pages.type"] == "TEMPLATE") {
			// Template Thymeleaf structure
			if (request.requestURI.isEmpty() || request.requestURI.trim() == "/") {
				return "index"
			}

			return request.requestURI.removePrefix("/")
		}

		// Single page application
		return "index"
	}
}