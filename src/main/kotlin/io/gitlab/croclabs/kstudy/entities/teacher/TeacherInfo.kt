package io.gitlab.croclabs.kstudy.entities.teacher

import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile
import org.springframework.data.rest.core.config.Projection
import java.util.*

/**
 * Projection for {@link io.gitlab.croclabs.kstudy.entities.teacher.Teacher}
 */
@Projection(name = "teacherInfo", types = [Teacher::class])
interface TeacherInfo {
	val id: UUID?
}
