package io.gitlab.croclabs.kstudy.entities.attribute.converter

import jakarta.persistence.AttributeConverter
import jakarta.persistence.Converter
import org.springframework.security.core.GrantedAuthority

@Converter
class AuthoritiesConverter : AttributeConverter<Collection<GrantedAuthority>, String> {
	override fun convertToDatabaseColumn(attribute: Collection<GrantedAuthority>): String {
		return attribute.joinToString(separator = ",") { it.authority }
	}

	override fun convertToEntityAttribute(dbData: String): Collection<GrantedAuthority> {
		return dbData.split(",").map {
			GrantedAuthority { it }
		}
	}
}