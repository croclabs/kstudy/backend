package io.gitlab.croclabs.kstudy.entities.userprofile

import io.gitlab.croclabs.kstudy.entities.attribute.converter.AuthoritiesConverter
import io.gitlab.croclabs.kstudy.entities.attribute.converter.PasswordConverter
import io.gitlab.croclabs.kstudy.entities.teacher.Teacher
import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.NotNull
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.engine.internal.Cascade
import org.hibernate.type.SqlTypes
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*
import kotlin.collections.ArrayList

@Entity
@Table(name = "user_profile")
open class UserProfile(
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	open var id: UUID? = null,

	@NotBlank
	@Column(name = "username", unique = true, nullable = false)
	open var username: String = "",

	@NotBlank
	@Convert(converter = PasswordConverter::class)
	@Column(name = "password", updatable = false, nullable = false)
	open var password: String = "",

	@NotBlank
	@Column(name = "firstname", nullable = false)
	open var firstname: String = "",


	@Column(name = "middlename", nullable = false)
	open var middlename: String? = "",

	@NotBlank
	@Column(name = "lastname", nullable = false)
	open var lastname: String = "",

	@NotNull
	@NotEmpty
	@Column(name = "authorities", nullable = false, length = 2000)
	@JdbcTypeCode(SqlTypes.VARCHAR)
	@Convert(converter = AuthoritiesConverter::class)
	open var authorities: List<GrantedAuthority> = ArrayList(),

	@NotNull
	@Column(name = "expired", nullable = false)
	open var expired: Boolean = false,

	@NotNull
	@Column(name = "locked", nullable = false)
	open var locked: Boolean = false,

	@NotNull
	@Column(name = "credentials_expired", nullable = false)
	open var credentialsExpired: Boolean = false,

	@NotNull
	@Column(name = "enabled", nullable = false)
	open var enabled: Boolean = false,

	@OneToOne(cascade = [CascadeType.ALL])
	@JoinColumn(name = "teacher_id", referencedColumnName = "id")
	open var teacher: Teacher? = null,
) {
	fun getType(): Type = if (teacher != null) Type.TEACHER else Type.UNKNOWN

	enum class Type {
		UNKNOWN, TEACHER, STUDENT
	}
}