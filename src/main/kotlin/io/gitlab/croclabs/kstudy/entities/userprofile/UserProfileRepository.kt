package io.gitlab.croclabs.kstudy.entities.userprofile;

import jakarta.transaction.Transactional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource
import org.springframework.stereotype.Repository
import java.util.*
import kotlin.reflect.KClass

@RepositoryRestResource(path = "users", excerptProjection = UserProfileInfo::class)
interface UserProfileRepository : JpaRepository<UserProfile, UUID> {
	@Query(
		"""
		UPDATE UserProfile u
		SET u.password = :password
		WHERE u.id = :id
		"""
	)
	@Modifying
	@Transactional
	fun updatePassword(id: UUID?, password: String?): Int

	fun <T> findById(id: UUID?, clazz: Class<T>?): T
	fun findByUsername(username: String?): UserProfile
	fun <T> findByUsername(username: String?, clazz: Class<T>): T
}