package io.gitlab.croclabs.kstudy.entities.teacher

import org.springframework.security.core.GrantedAuthority
import java.util.*

/**
 * Projection for {@link io.gitlab.croclabs.kstudy.entities.teacher.Teacher}
 */
interface TeacherProfileInfo {
	val id: UUID
	val userProfile: UserProfileInfo

	/**
	 * Projection for {@link io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile}
	 */
	interface UserProfileInfo {
		val id: UUID
		val username: String
		val firstname: String
		val middlename: String?
		val lastname: String
		val authorities: List<GrantedAuthority>
		val expired: Boolean
		val locked: Boolean
		val credentialsExpired: Boolean
		val enabled: Boolean
	}
}
