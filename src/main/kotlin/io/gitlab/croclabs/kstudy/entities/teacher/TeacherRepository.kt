package io.gitlab.croclabs.kstudy.entities.teacher;

import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TeacherRepository : JpaRepository<Teacher, UUID>