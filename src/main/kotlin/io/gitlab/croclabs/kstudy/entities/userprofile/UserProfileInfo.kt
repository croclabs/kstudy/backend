package io.gitlab.croclabs.kstudy.entities.userprofile

import io.gitlab.croclabs.kstudy.entities.teacher.TeacherInfo
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.rest.core.config.Projection
import org.springframework.security.core.GrantedAuthority
import java.util.*

/**
 * Projection for {@link io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile}
 */
@Projection(name = "userProfileInfo", types = [UserProfile::class])
interface UserProfileInfo {
	val id: UUID
	val firstname: String
	val lastname: String
	val authorities: List<GrantedAuthority>

	@get:Value("#{target.firstName} #{target.lastName}")
	val fullname: String
	@get:Value("#{target.teacher.id}")
	val teacherId: UUID?
}
