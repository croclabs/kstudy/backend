package io.gitlab.croclabs.kstudy.entities.teacher

import io.gitlab.croclabs.kstudy.entities.attribute.converter.PasswordConverter
import io.gitlab.croclabs.kstudy.entities.userprofile.UserProfile
import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import org.springframework.security.core.userdetails.UserDetails
import java.util.UUID

@Entity
@Table(name = "teacher")
open class Teacher(
	@Id
	@NotNull
	@GeneratedValue(strategy = GenerationType.UUID)
	@Column(name = "id", nullable = false)
	open var id: UUID? = null,

	@OneToOne(mappedBy = "teacher")
	open var userProfile: UserProfile
)